(function ($) {
  'use strict';
  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  $(window).on('resize scroll', function () {
    $('.bh-slide-gallery-section').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('bh-start-animation');
      } else {
        $(this).removeClass('bh-start-animation');
      }
    });
  });

  var lastScrollTop = 0;
  $(window).scroll(function (event) {
    var st = $(this).scrollTop();
    if (st >= $('.header-fixed').height()) {
      $('.header-fixed').addClass('fixed').addClass('scroll-down');
    } else {
      $('.header-fixed')
        .removeClass('fixed')
        .removeClass('scroll-down')
        .removeClass('scroll-up');
    }
    if (st > lastScrollTop) {
      $('.header-fixed.fixed').removeClass('scroll-up').addClass('scroll-down');
    } else {
      $('.header-fixed.fixed').removeClass('scroll-down').addClass('scroll-up');
    }
    lastScrollTop = st;
  });
  function isOverflown(element) {
    return (
      element.scrollHeight > element.clientHeight ||
      element.scrollWidth > element.clientWidth
    );
  }
  var width =
    $('.bh-slide-gallery-wrap').width() +
    parseFloat($('.bh-slide-gallery-wrap').css('column-gap'));
  $('.bh-sliding-button-stop').click(function () {
    $(this).parent().toggleClass('bh-pause-animation');
    $(this).find('i').toggleClass('la-play la-pause');
    return false;
  });
  $('.bh-slide-gallery-wrap')
    .find('.bh-slide-gallery-item')
    .each(function () {
      if (!$(this).hasClass('cloned')) {
        $(this)
          .clone()
          .addClass('cloned')
          .appendTo($('.bh-slide-gallery-wrap'));
      }
    });
  var animateSliding = function () {
    $('.bh-slide-gallery-wrap')
      .css({ now: '0' })
      .animate(
        { now: width },
        {
          start: function () {
            var wrap = $(this);
            $(this)
              .find('.bh-slide-gallery-item')
              .each(function () {
                if (!$(this).hasClass('cloned')) {
                  $(this).addClass('cloned').clone().appendTo(wrap);
                }
              });
          },
          step: function (now, fx) {
            $(this).css('transform', 'translateX(-' + now + 'px)');
          },
          complete: function () {
            $(this).css('transform', 'translateX(0%)');
            animateSliding();
          },
          duration: 10000,
        },
        'linear'
      );
  };

  $('.slider-field .slider').on('input', function () {
    $(this)
      .parent()
      .next()
      .text(parseFloat($(this).val()).toFixed(1));
  });
  $('.widget_nav_menu .menu .menu-item').on('click', function (e) {
    var submenu = $(this).children('.sub-menu');
    var parent_submenu = $(this).parent();
    if (e.target.tagName.toLowerCase() === 'a') return;
    submenu.toggleClass('sub-menu-show'); //then show the current submenu
    if (submenu.hasClass('sub-menu-show')) {
      $('.widget_nav_menu .menu').css('height', submenu.height() + 'px');
    } else {
      $('.widget_nav_menu .menu').css('height', parent_submenu.height() + 'px');
    }
    if (!$('.sub-menu').hasClass('sub-menu-show')) {
      $('.widget_nav_menu .menu').css('height', 'auto');
    } else {
    }
    e.stopPropagation();
    e.preventDefault();
  });
  function isInArray(value, array) {
    return array.indexOf(value) > -1;
  }
  $('.bh-languages-dropdown li a').click(function () {
    var short_name = $(this).data('lang');
    $(this)
      .parents('.languages-block')
      .find('.current-lang span')
      .text(short_name);
  });

  $('.bh-language-menu li a').click(function () {
    var short_name = $(this).data('lang');
    $(this)
      .parents('.bh-language-menu')
      .find('.current-lang span')
      .text(short_name);
    $(this).parents('.dl-submenu').find('.dl-back').click();
  });
  $(document).ready(function () {
    roomSearch.init();
    $('.remove-wishlist').on('click', function (e) {
      var wishlist = new Array(),
        userData = '';
      e.preventDefault();
      var $this = $(this);
      var currentProduct = $this.data('product');
      currentProduct = currentProduct.toString();
      var loggedIn = $('body').hasClass('logged-in') ? true : false;
      if (loggedIn) {
        $this.addClass('loading');
        $this.parents('.bh-room.bh-room-search-item').addClass('loading');
        // Fetch current user data
        $.ajax({
          type: 'POST',
          url: opt.ajaxUrl,
          data: {
            action: 'fetch_user_data',
            dataType: 'json',
          },
          success: function (data) {
            userData = JSON.parse(data);
            if (
              typeof userData['wishlist'] != 'undefined' &&
              userData['wishlist'] != null &&
              userData['wishlist'] != ''
            ) {
              var userWishlist = userData['wishlist'];
              userWishlist = userWishlist.split(',');
              wishlist = userWishlist;
            }
            var index = wishlist.indexOf(currentProduct);
            if (index > -1) {
              wishlist.splice(index, 1);
            }
            $.ajax({
              type: 'POST',
              url: opt.ajaxPost,
              data: {
                action: 'user_wishlist_update',
                user_id: userData['user_id'],
                wishlist: wishlist.join(','),
              },
            })
              .done(function (response) {
                $this
                  .parents('.bh-room.bh-room-search-item')
                  .slideUp(300)
                  .delay(300)
                  .remove();
              })
              .fail(function (data) {
                $this
                  .parents('.bh-room.bh-room-search-item')
                  .slideUp(300)
                  .delay(300)
                  .remove();
              });
          },
          error: function () {},
        });
      }
    });
  });

  $('.elementor-image-gallery').find('style').remove();
  $('.elementor-image-gallery .gallery').removeAttr('id');
  $(window).load(function () {
    if (typeof opt != 'undefined' && opt !== null) {
      roomWishlist.init();
      roomWishlist.triggerWishlist('.single-room-save');
    }

    if ($('#bh-room-featured-carousel .bh-room-fc-wrap').length) {
      $('#bh-room-featured-carousel .bh-room-fc-wrap.owl-carousel').trigger(
        'refresh.owl.carousel'
      );
    }
  });
  function unique(array) {
    return $.grep(array, function (el, index) {
      return index == $.inArray(el, array);
    });
  }
  var roomWishlist = {
    wishlist: new Array(),
    loggedIn: $('body').hasClass('logged-in') ? true : false,
    userData: '',
    init: function () {
      var restUrl = opt.restUrl,
        shopName = opt.shopName + '-wishlist',
        inWishlist = opt.inWishlist,
        ls = localStorage.getItem(shopName),
        wishlist = roomWishlist.wishlist;
      roomWishlist.loggedIn = $('body').hasClass('logged-in') ? true : false;
      if (roomWishlist.loggedIn) {
        // Fetch current user data
        $.ajax({
          type: 'POST',
          url: opt.ajaxUrl,
          data: {
            action: 'fetch_user_data',
            dataType: 'json',
          },
          success: function (data) {
            roomWishlist.userData = JSON.parse(data);

            if (
              typeof roomWishlist.userData['wishlist'] != 'undefined' &&
              roomWishlist.userData['wishlist'] != null &&
              roomWishlist.userData['wishlist'] != ''
            ) {
              var userWishlist = roomWishlist.userData['wishlist'];
              userWishlist = userWishlist.split(',');

              if (wishlist.length) {
                wishlist = wishlist.concat(userWishlist);

                $.ajax({
                  type: 'POST',
                  url: opt.ajaxPost,
                  data: {
                    action: 'user_wishlist_update',
                    user_id: roomWishlist.userData['user_id'],
                    wishlist: wishlist.join(','),
                  },
                });
              } else {
                wishlist = userWishlist;
              }

              wishlist = unique(wishlist);
              roomWishlist.highlightWishlist(wishlist, inWishlist);
              localStorage.removeItem(shopName);
            } else {
              if (typeof ls != 'undefined' && ls != null) {
                ls = ls.split(',');
                ls = unique(ls);
                wishlist = ls;
              }

              $.ajax({
                type: 'POST',
                url: opt.ajaxPost,
                data: {
                  action: 'user_wishlist_update',
                  user_id: roomWishlist.userData['user_id'],
                  wishlist: wishlist.join(','),
                },
              }).done(function (response) {
                roomWishlist.highlightWishlist(wishlist, inWishlist);
                localStorage.removeItem(shopName);
              });
            }
            roomWishlist.wishlist = wishlist;
          },
          error: function () {},
        });
      } else {
        if (typeof ls != 'undefined' && ls != null) {
          ls = ls.split(',');
          ls = unique(ls);
          wishlist = ls;
          roomWishlist.highlightWishlist(wishlist, inWishlist);
          roomWishlist.wishlist = wishlist;
        }
      }
    },
    onWishlistComplete: function (target, title) {
      if (target.attr('title')) {
        title = '';
      }
      target.removeClass('loading').toggleClass('active').attr('title', title);
    },
    isInArray: function (value, array) {
      return array.indexOf(value) > -1;
    },
    highlightWishlist: function (wishlist, title) {
      $('.single-room-save').each(function () {
        var $this = $(this);
        var currentProduct = $this.data('product');
        currentProduct = currentProduct.toString();
        if (roomWishlist.isInArray(currentProduct, wishlist)) {
          $this.addClass('active').attr('title', title);
        }
      });
    },
    triggerWishlist: function (button) {
      $('body').on('click', button, function (e) {
        var shopName = opt.shopName + '-wishlist',
          inWishlist = opt.inWishlist,
          ls = localStorage.getItem(shopName);
        e.preventDefault();
        var $this = $(this);
        var currentProduct = $this.data('product');
        currentProduct = currentProduct.toString();

        roomWishlist.loggedIn = $('body').hasClass('logged-in') ? true : false;

        $this.addClass('loading');
        if ($this.parent('bh-room').length) {
          $this.parent('bh-room').addClass('loading');
        }
        if (roomWishlist.loggedIn) {
          // Fetch current user data
          $.ajax({
            type: 'POST',
            url: opt.ajaxUrl,
            data: {
              action: 'fetch_user_data',
              dataType: 'json',
            },
            success: function (data) {
              roomWishlist.userData = JSON.parse(data);
              if (
                typeof roomWishlist.userData['wishlist'] != 'undefined' &&
                roomWishlist.userData['wishlist'] != null &&
                roomWishlist.userData['wishlist'] != ''
              ) {
                var userWishlist = roomWishlist.userData['wishlist'];
                userWishlist = userWishlist.split(',');
                roomWishlist.wishlist = userWishlist;
              } else {
                roomWishlist.wishlist = new Array();
              }
              if (
                !roomWishlist.isInArray(currentProduct, roomWishlist.wishlist)
              ) {
                roomWishlist.wishlist.push(currentProduct);
              } else {
                var index = roomWishlist.wishlist.indexOf(currentProduct);
                if (index > -1) {
                  roomWishlist.wishlist.splice(index, 1);
                }
              }

              var wishlist = unique(roomWishlist.wishlist);

              $.ajax({
                type: 'POST',
                url: opt.ajaxPost,
                data: {
                  action: 'user_wishlist_update',
                  user_id: roomWishlist.userData['user_id'],
                  wishlist: wishlist.join(','),
                },
              })
                .done(function (response) {
                  roomWishlist.onWishlistComplete($this, inWishlist);
                })
                .fail(function (data) {});
            },
            error: function () {},
          });
        } else {
          if (typeof ls != 'undefined' && ls != null) {
            ls = ls.split(',');
            ls = unique(ls);
            roomWishlist.wishlist = ls;
            if (
              !roomWishlist.isInArray(currentProduct, roomWishlist.wishlist)
            ) {
              roomWishlist.wishlist.push(currentProduct);
            } else {
              var index = roomWishlist.wishlist.indexOf(currentProduct);
              if (index > -1) {
                roomWishlist.wishlist.splice(index, 1);
              }
            }
            roomWishlist.wishlist = unique(roomWishlist.wishlist);
          }
          localStorage.setItem(shopName, roomWishlist.wishlist.toString());
          setTimeout(function () {
            roomWishlist.onWishlistComplete($this, inWishlist);
          }, 800);
        }
      });
    },
  };
  var roomSearch = {
    init: function () {
      roomSearch.initCustomQuantity();
      roomSearch.searchActiveField();
      roomSearch.guestDropdownTriggers(
        $('#bh-room-reservation-search-form .bh-block-guests')
      );

      // Trigger function recursion on input change
      var personsInput = $(
        '#bh-room-reservation-search-form .bh-block-guests .bh-dropdown-persons'
      ).find('input');
      personsInput.on('change', function () {
        roomSearch.updateGuests(
          $('#bh-room-reservation-search-form .bh-block-guests')
        );
      });
      $(document).on('click', function (event) {
        var $target = $(event.target);
        if (
          !$target.closest('#bh-room-reservation-search-form').length &&
          $('#bh-room-reservation-search-form').hasClass('activated')
        ) {
          $('#bh-room-reservation-search-form').removeClass('activated');
          $('#bh-room-reservation-search-form')
            .find('.bh-field-selection')
            .removeClass('bh-field-active');
        }
      });
    },
    searchActiveField: function () {
      var inputs = $('#bh-room-reservation-search-form').find('input');
      if (inputs.length) {
        inputs.on('click', function () {
          $('#bh-room-reservation-search-form').addClass('activated');
          $('#bh-room-reservation-search-form .bh-field-selection').removeClass(
            'bh-field-active'
          );
          $(this).parents('.bh-field-selection').toggleClass('bh-field-active');
        });
      }
    },
    initCustomQuantity: function () {
      $('.bh-quantity').each(function () {
        var spinner = $(this),
          input = spinner.find('input[type="number"]'),
          btnUp = spinner.find('.bh-quantity-up'),
          btnDown = spinner.find('.bh-quantity-down'),
          min = input.attr('min'),
          max = input.attr('max');
        if (input.val() <= min) {
          btnDown.addClass('not-active');
        }
        if (input.val() >= max) {
          btnUp.addClass('not-active');
        }

        btnUp.on('click', function () {
          var oldValue = parseFloat(input.val());
          (min = input.attr('min')), (max = input.attr('max'));
          if (oldValue >= max) {
            var newVal = oldValue;
          } else {
            var newVal = oldValue + 1;
          }
          if (oldValue >= max || newVal >= max) {
            btnUp.addClass('not-active');
          }
          btnDown.removeClass('not-active');
          spinner.find('input').val(newVal);
          spinner.find('input').trigger('change');
        });

        btnDown.on('click', function () {
          var oldValue = parseFloat(input.val());
          (min = input.attr('min')), (max = input.attr('max'));
          btnUp.removeClass('not-active');
          if (oldValue <= min) {
            var newVal = oldValue;
            btnDown.addClass('not-active');
          } else {
            var newVal = oldValue - 1;
          }
          if (oldValue <= min || newVal <= min) {
            btnDown.addClass('not-active');
          }

          spinner.find('input').val(newVal);
          spinner.find('input').trigger('change');
        });
      });
    },
    guestDropdownTriggers: function (guests) {
      var guestsDropdown = guests.find('.bh-dropdown-persons');

      guests.find('input.bh--guests').on('click', function (e) {
        roomSearch.guestDropdownOpen(guestsDropdown);
        roomSearch.updateGuests(guests);
      });

      guests.find('.bh-persons--close').on('click', function (e) {
        e.preventDefault();
        roomSearch.guestDropdownOpen(guestsDropdown);
      });

      guests.find('.bh-dropdown-persons-close').on('click', function (e) {
        guestsDropdown.removeClass('bh--opened');
        $(this).parent().parent().removeClass('bh-field-active');
        $(this)
          .parents('#bh-room-reservation-search-form')
          .removeClass('activated');
      });

      // Close on other input trigger
      guests
        .siblings()
        .find('input, select, .select2')
        .on('click', function (e) {
          if (guestsDropdown.hasClass('bh--opened')) {
            roomSearch.guestDropdownOpen(guestsDropdown);
          }
        });
      // Close on escape
      $(document).keyup(function (e) {
        if (e.keyCode === 27 && guestsDropdown.hasClass('bh--opened')) {
          // KeyCode for ESC button is 27
          roomSearch.guestDropdownOpen(guestsDropdown);
        }
      });

      $(document).on('click', function (e) {
        if (
          !$(e.target).closest('.bh-block-guests').length &&
          guestsDropdown.hasClass('bh--opened')
        ) {
          roomSearch.guestDropdownOpen(guestsDropdown);
        }
      });
    },
    guestDropdownOpen: function (block) {
      var itemTopOffset = block.offset().top,
        itemHeight = block.outerHeight(),
        windowHeight = $(window).height(),
        windowScroll = $(window).scrollTop();
      // Set item position
      if (itemTopOffset + itemHeight > windowHeight + windowScroll) {
        block.addClass('bh--above');
      } else {
        block.removeClass('bh--above');
      }

      // Set item visibility
      if (block.hasClass('bh--opened')) {
        block.removeClass('bh--opened');
      } else {
        block.addClass('bh--opened');
      }
    },
    updateGuests: function (guests) {
      var guestsInput = guests.find('.bh-field-input.bh--guests'),
        guestsValue = '',
        totalCount = 0,
        guestsCount = 0,
        maxGuestsCount = guestsInput.data('max'),
        curGuestsCount = guestsInput.data('count'),
        persons = guests.find('.bh-dropdown-persons'),
        personsInput = persons.find('input');

      if (personsInput.length) {
        personsInput.each(function () {
          var input = $(this),
            inputValue = input.val(),
            singularLabel = input.data('singular-label'),
            pluralLabel = input.data('plural-label');

          if (inputValue > 0) {
            // Set separator between persons
            if (guestsValue.length) {
              guestsValue += ', ';
            }

            // Set new persons value
            guestsValue += inputValue;

            // Set persons count
            if (!input.parent().parent().hasClass('bh--infant')) {
              guestsCount += parseInt(inputValue, 10);
            }
            // Set singular or plural label after the number
            if (inputValue > 1) {
              guestsValue += ' ' + pluralLabel;
            } else {
              guestsValue += ' ' + singularLabel;
            }
            input.attr('data-count', inputValue);
          }
        });
      }
      // Update guests input field
      if (guestsValue.length) {
        guestsInput.val(guestsValue);
        guestsInput.data('count', guestsCount);
        guestsInput.next().val(guestsCount);
      } else {
        guestsInput.val('');
      }
    },
  };
})(jQuery);

(function () {
  const parent = document.querySelector('.bh-price-range-slider');
  if (!parent) {
    return;
  }
  const rangeS = parent.querySelectorAll('input[type="range"]'),
    numberS = parent.querySelectorAll('input[type="number"]');
  rangeS.forEach(el => {
    el.oninput = () => {
      let slide1 = parseFloat(rangeS[0].value),
        slide2 = parseFloat(rangeS[1].value);

      if (slide1 > slide2) {
        [slide1, slide2] = [slide2, slide1];
      }

      numberS[0].value = slide1;
      numberS[1].value = slide2;
    };
  });
  numberS.forEach(el => {
    el.oninput = () => {
      let number1 = parseFloat(numberS[0].value),
        number2 = parseFloat(numberS[1].value);

      if (number1 > number2) {
        let tmp = number1;
        numberS[0].value = number2;
        numberS[1].value = tmp;
      }

      rangeS[0].value = number1;
      rangeS[1].value = number2;
    };
  });
})();
jQuery(function ($) {
  var customCheckboxes = $('.bh-field-item-checkbox');
  if (customCheckboxes.length) {
    customCheckboxes
      .find('.bh-field-checkbox, .bh-field-label')
      .on('click', function () {
        var clickedItem = $(this),
          checkboxItem = clickedItem.parent(),
          inputField = checkboxItem.children('.bh-field-input');
        if (!inputField.is(':disabled')) {
          if (checkboxItem.hasClass('bh--checked')) {
            inputField.prop('checked', false);
            checkboxItem.removeClass('bh--checked');
          } else {
            inputField.prop('checked', true);
            checkboxItem.addClass('bh--checked');
          }
        }
      });
  }
  function filterGetResults() {
    var filter = $('#bh-room-filter-form');
    $.ajax({
      url: filter.data('request'),
      data: filter.serialize(), // form data
      type: filter.attr('method'), // POST
      beforeSend: function (xhr) {
        $('#bhresponse')
          .append('<div class="circle"></div>')
          .addClass('loading');
      },
      success: function (data) {
        $('#bhresponse')
          .html(data)
          .removeClass('loading')
          .find('.circle')
          .remove(); // insert data
      },
    });
  }

  $('#bh-room-filter-form .bh-field-item-checkbox').on('click', function () {
    filterGetResults();
  });
  $('.bh-price-range-inputs input, .bh-price-range-slider-wrap input').on(
    'change',
    function () {
      filterGetResults();
    }
  );
  $('body').on('click', '.reviews a.page-numbers', function (e) {
    e.preventDefault();
    var filter = $('#bh-room-filter-form');
    var paged = $(this).attr('href').split('#');
    $.ajax({
      url: filter.data('request'),
      data: filter.serialize() + '&pagenum=' + paged.pop(), // form data
      type: filter.attr('method'), // POST
      beforeSend: function (xhr) {
        $('#bhresponse')
          .append('<div class="circle"></div>')
          .addClass('loading');
      },
      success: function (data) {
        $('#bhresponse')
          .html(data)
          .removeClass('loading')
          .find('.circle')
          .remove(); // insert data
      },
    });
  });
  $(window).load(function () {
    $('.pac-container')
      .removeClass('pac-logo')
      .prependTo('#bh-room-reservation-search-form .bh-room-location');
  });
  if ($('#bh-location-suggestions').length) {
    var tmp_dd_cont = $('#bh-location-suggestions').html();
    function getLocationSuggestions(query, dropdown) {
      $.get(
        'https://nominatim.openstreetmap.org/search',
        {
          format: 'json',
          q: query,
          limit: 4,
          featuretype: 'city',
        },
        function (results) {
          if (!results.length) {
            dropdown.removeClass('bh-active').empty().append(tmp_dd_cont);
            return;
          }
          dropdown.empty();
          var itemHTML = '';
          results.map(function (item) {
            var address = item.display_name.replace(/,[^,]+$/, '');
            var country = item.display_name.split(', ').pop();
            itemHTML =
              '<div class="pac-item" data-address="' +
              item.display_name +
              '">' +
              '<span class="pac-icon pac-icon-marker"></span>' +
              '<span class="pac-item-query">' +
              address +
              '</span>' +
              '<span>' +
              country +
              '</span>' +
              '</div>';
            dropdown.append(itemHTML);
          });
          dropdown.addClass('bh-active');
        },
        'json'
      );
    }
    $('#bh-location-suggestions').on('click', '.pac-item', function () {
      var input = $('#bh-location-suggestions')
        .next()
        .find('#bh-room-location');
      input.val($(this).data('address'));
      $('#bh-location-suggestions').parent().removeClass('bh-field-active');
    });
    $('#bh-room-location').on('input', function () {
      $(this).attr('placeholder', $(this).data('search-ex'));
      if ($(this).val().length > 2) {
        getLocationSuggestions($(this).val(), $('#bh-location-suggestions'));
      } else {
        $('#bh-location-suggestions').empty().append(tmp_dd_cont);
      }
    });
  }
  $('.bh-field-selection:not(.bh-block-guests):not(.bh-block-rooms)').on(
    'click',
    function () {
      $('.bh-field-selection').removeClass('bh-field-active');
      $(this).toggleClass('bh-field-active').find('input').get(0).click();
    }
  );
  $(document).on('click', '#close-check-in', function () {
    $('.bh-field-selection').removeClass('bh-field-active');
  });
  if ($('#bh-room-featured-carousel').length) {
    $('#bh-room-featured-carousel .bh-room-fc-wrap').owlCarousel({
      margin: 10,
      loop: true,
      autoWidth: true,
      items: 2,
      dots: false,
      nav: true,
      navText: [
        '<i class="la la-angle-left"></i>',
        '<i class="la la-angle-right"></i>',
      ],
      navElement: 'div',
      lazyLoad: true,
    });
  }

  $('.bh-block-rooms')
    .find('.bh-dropdown-rooms-close')
    .on('click', function (e) {
      $(this).parent().parent().removeClass('bh-field-active');
      $(this)
        .parents('#bh-room-reservation-search-form')
        .removeClass('activated');
    });
  $('.bh-block-rooms .bh--rooms-count')
    .find('input')
    .on('change', function () {
      $(this)
        .parents('.bh-block-rooms')
        .find('#bh-rooms-count')
        .val($(this).val());
    });

  // Show the login dialog box on click
  if ($('#bh-popup-login').length) {
    $('#login.user-link a').on('click', function (e) {
      $('#bh-popup-login').addClass('active');
      $('form#login a.close, form#register a.close').on('click', function () {
        $('#bh-popup-login').removeClass('active');
      });
      e.preventDefault();
    });
    $('.registration-link a').on('click', function (e) {
      $(this).parents('#bh-popup-login').find('form').toggleClass('disable');
      e.preventDefault();
    });
  }
  // Close on escape
  $(document).keyup(function (e) {
    if (e.keyCode === 27 && $('#bh-popup-login').hasClass('active')) {
      // KeyCode for ESC button is 27
      $('#bh-popup-login').removeClass('active');
    }
  });
  // Perform AJAX login on form submit
  $('form#login, form#register').on('submit', function (e) {
    currentForm = $(this);
    currentForm
      .find('p.bh-response')
      .addClass('bh--show')
      .text(ajax_login_object.loadingmessage);
    action = 'ajaxlogin';
    username = $('form#login #username').val();
    password = $('form#login #password').val();
    email = lastname = firstname = '';
    security = $('form#login #security').val();
    redirect = ajax_login_object.redirecturl;
    if ($(this).attr('id') == 'register') {
      action = 'ajaxregister';
      firstname = $('form#register #firstname').val();
      lastname = $('form#register #lastname').val();
      password = $('form#register #signonpassword').val();
      email = $('form#register #signonemail').val();
      security = $('form#register #signonsecurity').val();
      redirect = ajax_login_object.reg_redirecturl;
    }
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: ajax_login_object.ajaxurl,
      data: {
        action: action, //calls wp_ajax_nopriv_ajaxlogin
        username: username,
        password: password,
        security: security,
        email: email,
        lastname: lastname,
        firstname: firstname,
      },
      success: function (data) {
        if (data.loggedin == false) {
          currentForm
            .find('p.bh-response')
            .addClass('bh--error')
            .text(data.message);
        } else if (data.loggedin == true && data.redirect) {
          currentForm
            .find('p.bh-response')
            .removeClass('bh--error')
            .addClass('bh--success')
            .text(data.message);
          setTimeout(function () {
            document.location.href = data.redirect;
          }, 200);
        } else if (data.loggedin == true) {
          currentForm
            .find('p.bh-response')
            .removeClass('bh--error')
            .addClass('bh--success')
            .text(data.message);
          setTimeout(function () {
            document.location.href = redirect;
          }, 200);
        }
      },
    });
    e.preventDefault();
  });
  $(document).on('click', '#checkout_apply_coupon', function () {
    // Get the coupon code
    var code = $('#checkout_coupon_code').val(),
      button = $(this),
      data = {
        action: 'ajaxapplucoupon',
        coupon_code: code,
      };
    // Send it over to WordPress.
    $.post(wc_checkout_params.ajax_url, data, function (returned_data) {
      if (returned_data.result == 'error') {
        button.next().html(returned_data.message);
      } else {
        setTimeout(function () {
          //reload with ajax
          $(document.body).trigger('update_checkout');
        }, 1000);
      }
    });
  });
  // The parallax function
  var bht_parallax = function (selector) {
    var multiplier = selector.data('bh-parallax-speed');
    multiplier = typeof multiplier !== 'undefined' ? multiplier : 0.2;
    var elementOffsetTop = selector.offset().top,
      windowHeight = $(window).height();

    $(window).scroll(function () {
      if (selector.isInViewport()) {
        var viewportTop = $(window).scrollTop(),
          viewportBottom = viewportTop + windowHeight,
          scrolled = windowHeight / (viewportBottom - elementOffsetTop),
          translateYCss = (1 / scrolled) * 100 * multiplier + 'px';
        selector.css('transform', 'translateY(' + translateYCss + ')');
      }
    });
  };

  var bht_video_resize_on_scroll = function (element) {
    var elementOffsetTop = element.offset().top,
      elementOffsetBottom = elementOffsetTop + element.outerHeight(),
      windowHeight = $(window).height();

    $(window).scroll(function () {
      var viewportTop = $(window).scrollTop(),
        viewportBottom = viewportTop + windowHeight;
      if (element.isInViewport() && elementOffsetBottom > viewportBottom) {
        var scrolled =
            ((viewportBottom - elementOffsetTop) * 5.5) / element.outerHeight(),
          scrolled = scrolled < 5.4 ? scrolled : 5.4;
        translateYCss = scrolled + '%';
        element.css('clip-path', 'inset(0px ' + translateYCss + ')');
      }
    });
  };
  $(document).ready(function () {
    $('.page-loading').fadeOut('fast').remove();
    $('.bh-back-to-top a').click(function () {
      $('html, body').animate({ scrollTop: 0 }, '800');
      return false;
    });
    if ($('.bh-cookie-bar').length) {
      let cookieLaw = {
        bar: '.bh-cookie-bar',
        button: '.bh-cookie-bar__button',
        close: '.bh-cookie-bar__close',
        name: 'bht_cookie_law_info',
        init: function (e) {
          const c = document.querySelector(cookieLaw.close);
          const btn = document.querySelector(cookieLaw.button);
          c.addEventListener('click', cookieLaw.hide, false);
          btn.addEventListener('click', cookieLaw.set, false);
        },
        hide: function (e) {
          e.preventDefault();
          var b = document.querySelector(cookieLaw.bar);
          b.classList.add('hide');
        },
        set: function (e) {
          e.preventDefault();
          localStorage.setItem(cookieLaw.name, '1');
          cookieLaw.hide();
          const value = 'yes';
          let host_name = window.location.hostname;
          document.cookie =
            cookieLaw.name +
            '=' +
            value +
            '; path=/; domain=.' +
            host_name +
            ';';
          if (host_name.indexOf('www') != 1) {
            var host_name_withoutwww = host_name.replace('www', '');
            document.cookie =
              cookieLaw.name +
              '=' +
              value +
              '; path=/; domain=' +
              host_name_withoutwww +
              ';';
          }
          host_name = host_name.substring(
            host_name.lastIndexOf('.', host_name.lastIndexOf('.') - 1)
          );
          document.cookie =
            cookieLaw.name +
            '=' +
            value +
            '; path=/; domain=' +
            host_name +
            ';';
        },
      };
      cookieLaw.init();
    }
    $('.bh-parallax').each(function () {
      bht_parallax($(this));
    });

    $('.bh-video-block').each(function () {
      bht_video_resize_on_scroll($(this).find('.bh-video-preview'));
    });

    var mouseX = window.innerWidth / 2,
      mouseY = window.innerHeight / 2;

    var circle = {
      el: $('.bh-cursor'),
      x: window.innerWidth / 2,
      y: window.innerHeight / 2,
      update: function () {
        l = this.x;
        t = this.y;
        this.el.css({ left: l + 'px', top: t + 'px' });
      },
    };

    $(window).mousemove(function (e) {
      mouseX = e.clientX;
      mouseY = e.clientY;
      moveCustomCursor(e, '.bh-slider-block-wrap');
    });

    setInterval(move, 1000 / 60);
    function move() {
      circle.x = lerp(circle.x, mouseX, 0.1);
      circle.y = lerp(circle.y, mouseY, 0.1);
      circle.update();
    }

    function lerp(start, end, amt) {
      return (1 - amt) * start + amt * end;
    }

    function moveCustomCursor(e, selector) {
      const mouseY = e.clientY;
      const mouseX = e.clientX;
      const sliderWrapper = e.target.closest(selector);
      if (sliderWrapper) {
        const cursorSlider = sliderWrapper.querySelector('.bh-cursor');
        let {
          width: sliderWidth,
          height: sliderHeight,
          x: sliderX,
          y: sliderY,
        } = sliderWrapper.getBoundingClientRect();
        cursorSlider.classList.remove('arrow-right');
        cursorSlider.classList.add('arrow-left');
        if (mouseX - sliderX > sliderWidth / 2) {
          cursorSlider.classList.remove('arrow-left');
          cursorSlider.classList.add('arrow-right');
        }
      }
    }

    $('.bh-accordion .elementor-accordion-item > div').removeClass(
      'elementor-active'
    );
    $('.bh-accordion .elementor-accordion-item .elementor-tab-content').css(
      'display',
      'none'
    );
    if ($('.bh-video-preview').length) {
      $(document).on('click', '.bh-self-play', function (e) {
        e.preventDefault();
        $(this).addClass('loading');
        var videoPlayer = $(this)
          .parents('.bh-video-block')
          .find('#bh-video-html5');
        var videoSrc = videoPlayer.data('video');
        var videoHtml = '<video controls src="' + videoSrc + '"></video>';
        videoPlayer.append(videoHtml);
        var video = videoPlayer.find('video').get(0);
        setTimeout(() => {
          $(this).parent('.bh-video-preview').addClass('hide');
          video.play();
        }, 900);
      });
    }

    if ($('.bh-slider-block').length) {
      var owlSlider = $('.bh-slider-block');
      owlSlider.owlCarousel({
        margin: 0,
        loop: false,
        autoWidth: false,
        items: 1,
        dots: false,
        nav: false,
        smartSpeed: 700,
        autoHeight: true,
        lazyLoad: true,
      });

      $('.bh-slider-block.style2').each(function () {
        $(this).on('changed.owl.carousel', function (property) {
          var current = property.item.index;
          var currentNumb = $(property.target)
            .find('.owl-item')
            .eq(current)
            .find('.bh-current-slide');
          currentNumb.text(current + 1);
        });
      });
      $(document).on('click', '.bh-slider-block', function (e) {
        if (e.target.classList.contains('button')) {
          return;
        }
        var direction = $(this).siblings('.bh-slider-cursor');
        if (direction.hasClass('arrow-right')) {
          $(this).trigger('next.owl.carousel');
        } else if (direction.hasClass('arrow-left')) {
          $(this).trigger('prev.owl.carousel');
        }
      });
    }
  });
});

//load youtube video iframe api.
const tag = document.createElement('script');
const firstScriptTag = document.querySelector('script');
tag.src = 'https://www.youtube.com/iframe_api';
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//init video block when youtube iframe api loaded and ready to use.
window.onYouTubeIframeAPIReady = function () {
  videoPlayer('.bh-video-block-youtube');
};

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = Array.prototype.forEach;
}
//define init function for video block.
const videoPlayer = function (selector) {
  const videoContainer = document.querySelectorAll(selector);
  videoContainer.forEach(function (el) {
    const play = el.querySelector('.bh-yt-play');
    play.addEventListener('click', playVideo);
  });

  //define players array.
  let players = [];

  //callback for playbutton click.
  function playVideo(e) {
    e.preventDefault();

    //prevent button click on loading.
    if (e.currentTarget.classList.contains('loading')) {
      return;
    }

    const playButton = e.currentTarget;
    const video = playButton.parentNode.nextElementSibling;
    const imageOverlay = playButton.parentNode;
    playButton.classList.add('loading');

    const { youtube_id } = video.dataset;
    if (!youtube_id) {
      console.error('Add data-youtube_id attribute for #video-player');
      return;
    }

    //init new player.
    let player = new YT.Player(video, {
      height: '540',
      width: '960',
      videoId: youtube_id,
      events: {
        onReady: event => {
          // Hide the overlay and button and start playing the video.
          playButton.style.display = 'none';
          imageOverlay.style.display = 'none';

          //Start to play.
          event.target.playVideo();
        },
        onStateChange: event => {
          const playing = YT.PlayerState.PLAYING;
          if (event.data === playing) {
            for (const player of players) {
              if (
                player !== event.target &&
                player.getPlayerState() === playing
              ) {
                player.stopVideo();
              }
            }
          }
        },
      },
      playerVars: {
        rel: 0,
        showinfo: 0,
      },
      origin: window.location.href,
    });

    //add new player to array of players.
    players.push(player);
  }
};