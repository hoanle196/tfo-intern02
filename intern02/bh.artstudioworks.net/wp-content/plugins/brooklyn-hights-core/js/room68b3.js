function copyToClipboard(element) {
  var temp = jQuery('<input>');
  jQuery('body').append(temp);
  temp.val(jQuery(element).data('share')).select();
  document.execCommand('copy');
  temp.remove();
}

(function ($) {
  'use strict';
  $(document).ready(function () {
    roomReservation.init();
    if ($('#bh-search-date-range').length) {
      var checkIn = $('#bh-search-date-range').find('#check-in').get(0); //document.getElementById('check-in');
      var options = {
        format: 'DD-MM-YYYY',
        infoFormat: 'MMM DD, YYYY',
        moveBothMonths: true,
      };
      options.container = $('#bh-search-date-range').get(0);
      var search_dpkr = new HotelDatepicker(checkIn, options);
      options.container.click(function () {
        search_dpkr.open();
      });
    }
    Array.prototype.unique = function () {
      return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
    };
    $('.single-room-share').click(function () {
      $(this).toggleClass('active');
      return false;
    });
    //open and close single room gallery popup
    var popupGallery = $('#bh-room-popup-gallery');
    $('.bh-popup-gallery-open').click(function () {
      popupGallery.addClass('bh--opened');
      return false;
    });
    $('.bh-popup-gallery-close').click(function () {
      popupGallery.removeClass('bh--opened');
      return false;
    });
    $(document).keyup(function (e) {
      if (
        e.keyCode === 27 &&
        popupGallery.hasClass('bh--opened') &&
        $('.dialog-type-lightbox.elementor-lightbox').css('display') != 'block'
      ) {
        // KeyCode for ESC button is 27
        popupGallery.removeClass('bh--opened');
      }
    });
  });
  var roomReservation = {
    form: '',
    price: 0,
    nights: 1,
    priceField: '',
    guestsPrice: 0,
    extraPrice: 0,
    pricePerNight: 0,
    room_amount: 1,
    hdpkr: 0,
    roomCalendar: 0,
    totalPrice: 0,
    init: function () {
      //roomReservation.initCustomQuantity();
      roomReservation.priceField = $('#bh-room-reservation').find(
        '.bh--price .bh-price-value'
      );
      roomReservation.price =
        $('#bh-room-reservation').find('.bh--price input').data('room-price') ||
        0;

      (roomReservation.form = $('#bh-room-reservation-form')),
        (roomReservation.pricePerNight = roomReservation.price);
      if ($('#bh-rooms-count').length) {
        roomReservation.room_amount = $('#bh-rooms-count').val();
        $('input[name="bht_room_rooms"]').on('change', function () {
          roomReservation.room_amount = $(this).val();
          $('#bh-room-reservation')
            .find('input[name="quantity"]')
            .val($(this).val());
          roomReservation.calculateRequiredExtraServices();
        });
      }

      if ($('#bh-date-range').length) {
        roomReservation.initCalendar($('#bh-date-range'));
      }

      if ($('#bh-room-calendar').length) {
        roomReservation.initRoomCalendar($('#bh-room-calendar'));
      }

      roomReservation.updateGuests($('.bh-block-guests'));

      roomReservation.guestDropdownTriggers($('.bh-block-guests'));
      roomReservation.roomsDropdown();

      // Trigger function recursion on input change
      var personsInput = $('.bh-block-guests .bh-dropdown-persons').find(
        'input'
      );
      personsInput.on('change', function () {
        roomReservation.updateGuests($('.bh-block-guests'));
      });

      roomReservation.calculateRequiredExtraServices();
      roomReservation.setExtraServicesTriggers();

      roomReservation.triggerBooking();

      var submitButton = roomReservation.form.find('.bh--booking .button');
      roomReservation.nights = $('#bh-date-range').data('nights');
      roomReservation.updateCountNights(roomReservation.hdpkr);
      roomReservation.calculateExtraServices();
      if (
        $('#bh-date-range').find('#check-in').val() != '' &&
        $('#bh-date-range').find('#check-out').val() != ''
      ) {
        roomReservation.form.find('.bh--price').addClass('show');
        roomReservation.form.find('.bh--extra-services').addClass('show');
        submitButton.text(submitButton.data('reserve-text'));
      } else {
        roomReservation.form.find('.bh--price').removeClass('show');
        roomReservation.form.find('.bh--extra-services').removeClass('show');
        submitButton.text(submitButton.data('availability-text'));
      }
      roomReservation.loadMoreReviews();
    },
    initCustomQuantity: function () {
      $('.bh-quantity').each(function () {
        var spinner = $(this),
          input = spinner.find('input[type="number"]'),
          btnUp = spinner.find('.bh-quantity-up'),
          btnDown = spinner.find('.bh-quantity-down'),
          min = input.attr('min'),
          max = input.attr('max');

        btnUp.click(function () {
          var oldValue = parseFloat(input.val());
          (min = input.attr('min')), (max = input.attr('max'));
          if (oldValue >= max) {
            var newVal = oldValue;
          } else {
            var newVal = oldValue + 1;
          }

          spinner.find('input').val(newVal);
          spinner.find('input').trigger('change');
        });

        btnDown.click(function () {
          var oldValue = parseFloat(input.val());
          (min = input.attr('min')), (max = input.attr('max'));
          if (oldValue <= min) {
            var newVal = oldValue;
          } else {
            var newVal = oldValue - 1;
          }
          spinner.find('input').val(newVal);
          spinner.find('input').trigger('change');
        });
      });
    },
    initCalendar: function (calendar) {
      var checkIn = calendar.find('#check-in').get(0); //document.getElementById('check-in');
      var options = {
        format: 'DD-MM-YYYY',
        infoFormat: 'MMM DD, YYYY',
        moveBothMonths: true,
        autoClose: true,
      };
      if (BHGlobal.calI18n) {
        options.i18n = JSON.parse(BHGlobal.calI18n.trim());
      }
      options.container = calendar.get(0);
      options.disabledDates = calendar.data('disable-dates');
      options.minNights = calendar.data('min-nights');

      roomReservation.hdpkr = new HotelDatepicker(checkIn, options);
      $('.check-in-wrap, .check-out-wrap').on('click', function () {
        roomReservation.hdpkr.open();
      });
      var submitButton = roomReservation.form.find('.bh--booking .button');

      checkIn.addEventListener(
        'afterClose',
        function () {
          roomReservation.nights = roomReservation.hdpkr.getNights() || 1;
          roomReservation.calculateExtraServices();
          roomReservation.updateCountNights(roomReservation.hdpkr);
          if (roomReservation.hdpkr.start && roomReservation.hdpkr.end) {
            roomReservation.form.find('.bh--price').addClass('show');
            roomReservation.form.find('.bh--extra-services').addClass('show');
            submitButton.text(submitButton.data('reserve-text'));
          } else {
            roomReservation.form.find('.bh--price').removeClass('show');
            roomReservation.form
              .find('.bh--extra-services')
              .removeClass('show');
            submitButton.text(submitButton.data('availability-text'));
          }
        },
        false
      );

      roomReservation.hdpkr.onSelectRange = function () {
        if (roomReservation.hdpkr.end && roomReservation.roomCalendar) {
          var dateRangeValue =
            roomReservation.hdpkr.getDateString(
              new Date(roomReservation.hdpkr.start)
            ) +
            roomReservation.hdpkr.separator +
            roomReservation.hdpkr.getDateString(
              new Date(roomReservation.hdpkr.end)
            );
          // Set input value
          roomReservation.roomCalendar.setValue(
            dateRangeValue,
            roomReservation.hdpkr.getDateString(
              new Date(roomReservation.hdpkr.start)
            ),
            roomReservation.hdpkr.getDateString(
              new Date(roomReservation.hdpkr.end)
            )
          );
          roomReservation.roomCalendar.checkAndSetDefaultValue();
          roomReservation.roomCalendar.changed = true;

          roomReservation.updateRefundDay(roomReservation.hdpkr.start);
        }
      };
    },
    updateRefundDay: function (date) {
      var days = $('.bh-room-cancel-day');
      days.each(function (d) {
        var day = $(this);
        var dayCount = day.data('days-before');
        var dateObj = new Date(date);
        dateObj.setDate(dateObj.getDate() - dayCount);
        var lang = $('html').attr('lang') || 'en-US';
        day.text(
          dateObj.toLocaleDateString(lang, { month: 'short', day: 'numeric' })
        );
      });
    },
    initRoomCalendar: function (calendar) {
      var checkIn = calendar.find('#check-in-h').get(0); //document.getElementById('check-in');
      var options = {
        format: 'DD-MM-YYYY',
        infoFormat: 'MMM DD, YYYY',
        moveBothMonths: true,
        preventContainerClose: true,
        autoClose: false,
      };
      if (BHGlobal.calI18n) {
        options.i18n = JSON.parse(BHGlobal.calI18n.trim());
      }
      options.container = calendar.get(0);
      options.disabledDates = calendar.data('disable-dates');
      options.minNights = calendar.data('min-nights');
      roomReservation.roomCalendar = new HotelDatepicker(checkIn, options);
      roomReservation.roomCalendar.open();
      var submitButton = roomReservation.form.find('.bh--booking .button');
      roomReservation.roomCalendar.onSelectRange = function () {
        if (roomReservation.roomCalendar.end && roomReservation.hdpkr) {
          if (
            roomReservation.roomCalendar.start &&
            roomReservation.roomCalendar.end
          ) {
            roomReservation.form.find('.bh--price').addClass('show');
            roomReservation.form.find('.bh--extra-services').addClass('show');
            submitButton.text(submitButton.data('reserve-text'));
            var dateRangeValue =
              roomReservation.roomCalendar.getDateString(
                new Date(roomReservation.roomCalendar.start)
              ) +
              roomReservation.roomCalendar.separator +
              roomReservation.roomCalendar.getDateString(
                new Date(roomReservation.roomCalendar.end)
              );

            // Set input value
            roomReservation.hdpkr.setValue(
              dateRangeValue,
              roomReservation.roomCalendar.getDateString(
                new Date(roomReservation.roomCalendar.start)
              ),
              roomReservation.roomCalendar.getDateString(
                new Date(roomReservation.roomCalendar.end)
              )
            );
            roomReservation.hdpkr.changed = true;
            roomReservation.nights = roomReservation.hdpkr.getNights() || 1;
            roomReservation.calculateExtraServices();
            roomReservation.updateCountNights(roomReservation.hdpkr);
            roomReservation.form.find('.bh--price').addClass('show');
            roomReservation.updateRefundDay(roomReservation.roomCalendar.start);
          }
        }
      };
    },
    roomsDropdown: function () {
      $('#bh-room-reservation-form .bh-field.bh-block-rooms').on(
        'click',
        function (e) {
          if (!$(e.target).closest('.bh-dropdown-rooms').length) {
            $(this).toggleClass('bh-active-field');
            $(this).find('i').toggleClass('la-angle-up la-angle-down');
          }
        }
      );
      $(document).on('click', function (e) {
        if (!$(e.target).closest('.bh-block-rooms').length) {
          $('#bh-room-reservation-form .bh-field.bh-block-rooms').removeClass(
            'bh-active-field'
          );
          $('#bh-room-reservation-form .bh-field.bh-block-rooms')
            .find('i')
            .removeClass('la-angle-up')
            .addClass('la-angle-down');
        }
      });
      $(document).keyup(function (e) {
        if (
          e.keyCode === 27 &&
          $('#bh-room-reservation-form .bh-field.bh-block-rooms').hasClass(
            'bh-active-field'
          )
        ) {
          // KeyCode for ESC button is 27
          $('#bh-room-reservation-form .bh-field.bh-block-rooms').removeClass(
            'bh-active-field'
          );
          $('#bh-room-reservation-form .bh-field.bh-block-rooms')
            .find('i')
            .removeClass('la-angle-up')
            .addClass('la-angle-down');
        }
      });
    },
    guestDropdownTriggers: function (guests) {
      var guestsDropdown = guests.find('.bh-dropdown-persons');

      guests.find('input.bh--guests').on('click', function (e) {
        roomReservation.guestDropdownOpen(guestsDropdown);
      });
      guests
        .find('input.bh--guests')
        .next()
        .on('click', function (e) {
          roomReservation.guestDropdownOpen(guestsDropdown);
        });
      guests.find('.bh-field-label').on('click', function (e) {
        roomReservation.guestDropdownOpen(guestsDropdown);
      });
      guests.find('.bh-persons--close').on('click', function (e) {
        e.preventDefault();
        roomReservation.guestDropdownOpen(guestsDropdown);
      });

      // Close on other input trigger
      guests
        .siblings()
        .find('input, select, .select2')
        .on('click', function (e) {
          if (guestsDropdown.hasClass('bh--opened')) {
            roomReservation.guestDropdownOpen(guestsDropdown);
          }
        });

      // Close on escape
      $(document).keyup(function (e) {
        if (e.keyCode === 27 && guestsDropdown.hasClass('bh--opened')) {
          // KeyCode for ESC button is 27
          roomReservation.guestDropdownOpen(guestsDropdown);
        }
      });

      $(document).on('click', function (e) {
        if (
          !$(e.target).closest('.bh-block-guests').length &&
          guestsDropdown.hasClass('bh--opened')
        ) {
          roomReservation.guestDropdownOpen(guestsDropdown);
        }
      });
    },
    guestDropdownOpen: function (block) {
      var itemTopOffset = block.offset().top,
        itemHeight = block.outerHeight(),
        windowHeight = $(window).height(),
        windowScroll = $(window).scrollTop();
      // Set item position
      if (itemTopOffset + itemHeight > windowHeight + windowScroll) {
        block.addClass('bh--above');
      } else {
        block.removeClass('bh--above');
      }

      // Set item visibility
      if (block.hasClass('bh--opened')) {
        block.prev().find('i').toggleClass('la-angle-up la-angle-down');
        block.removeClass('bh--opened');
      } else {
        block.prev().find('i').toggleClass('la-angle-up la-angle-down');
        block.addClass('bh--opened');
      }
    },
    updateGuests: function (guests) {
      var guestsInput = guests.find('.bh-field-input.bh--guests'),
        guestsValue = '',
        guestsCount = 0,
        maxGuestsCount = guestsInput.data('max'),
        persons = guests.find('.bh-dropdown-persons'),
        personsInput = persons.find('input');

      if (personsInput.length) {
        personsInput.each(function () {
          var input = $(this),
            inputValue = input.val(),
            singularLabel = input.data('singular-label'),
            pluralLabel = input.data('plural-label');

          if (inputValue > 0) {
            // Set separator between persons
            if (guestsValue.length) {
              guestsValue += ', ';
            }

            // Set new persons value
            guestsValue += inputValue;

            // Set persons count
            if (!input.parent().parent().hasClass('bh--infant')) {
              input.attr('max', maxGuestsCount - guestsCount);
              guestsCount += parseInt(inputValue, 10);
            }
            // Set singular or plural label after the number
            if (inputValue > 1) {
              guestsValue += ' ' + pluralLabel;
            } else {
              guestsValue += ' ' + singularLabel;
            }
            input.attr('data-count', inputValue);
          }
          if (input.parent().parent().hasClass('bh--children')) {
            input
              .parent()
              .parent()
              .prev()
              .find('input')
              .attr('max', maxGuestsCount - parseInt(inputValue, 10));
          }
        });
      }
      // Update guests input field
      if (guestsValue.length) {
        guestsInput.val(guestsValue);
        guestsInput.data('count', guestsCount);
      }

      // Calculate a new price / night
      roomReservation.updateGuestsPrice(guestsCount);
    },
    updateGuestsPrice: function (guestsCount) {
      var form = roomReservation.form,
        minCapacity = parseInt(
          form.find('input[name="bht_room_min_capacity"]').val(),
          10
        );
      if (guestsCount >= minCapacity) {
        var adultInput = form.find('.bh--adult input'),
          adultsNumber = parseInt(adultInput.val(), 10),
          adultPrice = parseFloat(adultInput.data('price')),
          childrenInput = form.find('.bh--children input'),
          childrenNumber = parseInt(childrenInput.val(), 10),
          childrenPrice = parseFloat(childrenInput.data('price')),
          newPrice = 0;

        if (adultsNumber > minCapacity) {
          newPrice += adultPrice * (adultsNumber - minCapacity);
        }

        if (childrenNumber > 0) {
          if (adultsNumber <= minCapacity) {
            newPrice +=
              childrenPrice * (childrenNumber - minCapacity + adultsNumber);
          } else {
            newPrice += childrenPrice * childrenNumber;
          }
        }

        // Set a new price/night depend of guests number
        roomReservation.guestsPrice = newPrice;

        // Calculate a new room price
        roomReservation.calculateExtraServices();
      }
    },
    setExtraServicesTriggers: function () {
      var extraServices = $('.bh--extra-services .bh-field-item');

      if (extraServices.length) {
        extraServices
          .find('.bh-field-checkbox, .bh-field-label')
          .on('click', function () {
            var clickedItem = $(this),
              extraServiceItem = clickedItem.parent(),
              inputField = extraServiceItem.children('.bh-field-input'),
              type = inputField.data('service-type'),
              price = inputField.data('price');
            if (!inputField.is(':disabled')) {
              var increment = 1;

              if (extraServiceItem.hasClass('bh--checked')) {
                increment = -1;
                inputField.prop('checked', false);
                extraServiceItem.removeClass('bh--checked');
              } else {
                inputField.prop('checked', true);
                extraServiceItem.addClass('bh--checked');
              }

              if (type == 'per-night') {
                roomReservation.extraPrice += price * increment;
              } else if (type == 'subtract-per-night') {
                roomReservation.extraPrice -= price * increment;
              }
              // Calculate a new room price
              roomReservation.calculateExtraServices();
            }
          });
      }
    },
    getExtraServicePrice: function (item) {
      var totalPrice = 0,
        count = 0,
        price = item.data('price') || 0,
        nights = roomReservation.nights || 1,
        form = roomReservation.form,
        guestsInput = form.find('input[name="bht_room_guests"]'),
        adultInput = form.find('.bh--adult input'),
        childrenInput = form.find('.bh--children input'),
        infantInput = form.find('.bh--infant input');

      switch (item.data('service-type')) {
        case 'per-night':
          totalPrice = nights * parseFloat(price);
          break;
        case 'per-guest':
          count = Math.abs(guestsInput.data('count'));
          totalPrice = count * parseFloat(price);
          break;
        case 'per-adult':
          count = Math.abs(adultInput.data('count'));
          totalPrice = count * parseFloat(price);
          break;
        case 'per-children':
          count = Math.abs(childrenInput.data('count'));
          totalPrice = count * parseFloat(price);
          break;
        case 'per-infant':
          count = Math.abs(infantInput.data('count'));
          totalPrice = count * parseFloat(price);
          break;
        case 'subtract':
          totalPrice -= parseFloat(price);
          break;
        case 'subtract-per-night':
          totalPrice -= nights * parseFloat(price);
          break;
        default:
          totalPrice = parseFloat(price);
          break;
      }
      return totalPrice;
    },
    calculateRequiredExtraServices: function () {
      var extraServices = roomReservation.form.find(
        '.bh--extra-services .bh-field-input:disabled'
      );
      if (extraServices.length) {
        extraServices.each(function () {
          var currentItem = $(this);

          if (currentItem.data('service-type') == 'per-night') {
            roomReservation.extraPrice += currentItem.data('price');
          } else if (currentItem.data('service-type') == 'subtract-per-night') {
            roomReservation.extraPrice -= currentItem.data('price');
          }
        });
      }
      // Calculate a new room price
      roomReservation.calculateExtraServices();
    },
    calculateExtraServices: function () {
      var extraServices = roomReservation.form.find(
          '.bh--extra-services .bh-field-input:checked'
        ),
        extraPrice = 0;

      if (extraServices.length) {
        extraServices.each(function () {
          var currentItem = $(this);
          extraPrice += roomReservation.getExtraServicePrice(currentItem);
        });
      }
      // Calculate a new room price
      roomReservation.updatePrice(extraPrice);
    },
    updateCountNights: function (calendar) {
      var nights = roomReservation.nights,
        nightsText =
          nights === 1
            ? ' x ' + nights + ' ' + calendar.lang('night')
            : ' x ' + nights + ' ' + calendar.lang('nights');
      roomReservation.priceField.next().text(nightsText);
    },
    numberFormat: function (number, decimals, dec_point, thousands_sep) {
      // Strip all characters but numerical ones.
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = thousands_sep || ',',
        dec = dec_point || '.',
        s = '',
        toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    },
    updatePrice: function (extraPrice) {
      //roomReservation.calculateExtraServices();
      var priceField = roomReservation.priceField,
        pricePerNignt =
          roomReservation.pricePerNight +
          roomReservation.guestsPrice +
          roomReservation.extraPrice,
        extraPrice = extraPrice || 0,
        nights = roomReservation.nights || 1,
        totalPrice =
          extraPrice +
          parseFloat(pricePerNignt - roomReservation.extraPrice) * nights;
      roomReservation.totalPrice = totalPrice;

      roomReservation.form
        .find('.bh-price-total-value')
        .text(
          this.numberFormat(
            totalPrice * roomReservation.room_amount,
            BHGlobal.vars.priceFormat.decimals,
            BHGlobal.vars.priceFormat.decimalSeparator,
            BHGlobal.vars.priceFormat.thousandSeparator
          )
        );
      roomReservation.form.find('input[name="bht_room_price"]').val(totalPrice);

      priceField.text(pricePerNignt);
    },
    triggerBooking: function () {
      roomReservation.form.on('submit', function (e) {
        var form = $(this);

        if (!form.hasClass('bh--book-now')) {
          e.preventDefault();

          form.addClass('bh--checking');

          var responseHolder = form.find('.bh-response');

          responseHolder
            .removeClass('bh--show bh--success bh--error bh--undefined')
            .empty();

          var ajaxData = {
            room_id: form.find('input[name="add-to-cart"]').val(),
            check_in: form.find('input[name="bht_room_check_in"]').val(),
            check_out: form.find('input[name="bht_room_check_out"]').val(),
            adults: parseInt(form.find('[name="bht_room_adult"]').val(), 10),
            children: parseInt(
              form.find('[name="bht_room_children"]').val(),
              10
            ),
            infant: parseInt(form.find('[name="bht_room_infant"]').val(), 10),
            room_amount: parseInt(
              form.find('[name="bht_room_rooms"]').val(),
              10
            ),
            extra_services: '',
            min_capacity: parseInt(
              form.find('input[name="bht_room_min_capacity"]').val(),
              10
            ),
            max_capacity: parseInt(
              form.find('input[name="bht_room_max_capacity"]').val(),
              10
            ),
            price: parseFloat(roomReservation.totalPrice),
          };

          var extraServices = form.find(
            '.bh--extra-services .bh-field-input:checked'
          );

          if (extraServices.length) {
            var extraServicesItems = [];

            extraServices.each(function () {
              extraServicesItems.push($(this).val());
            });

            if (extraServicesItems.length) {
              ajaxData.extra_services = extraServicesItems.join(',');
            }
          }

          $.ajax({
            type: 'GET',
            url:
              BHGlobal.vars.restUrl + BHGlobal.vars.getRoomReservationRestRoute,
            data: {
              options: ajaxData,
            },
            beforeSend: function (request) {
              request.setRequestHeader('X-WP-Nonce', BHGlobal.vars.restNonce);
            },
            success: function (response) {
              responseHolder
                .addClass('bh--show bh--' + response.status)
                .html(response.message);

              if (response.status === 'success') {
                form.find('[name="bht_room_price"]').val(response.data.price);

                setTimeout(function () {
                  form.addClass('bh--book-now').trigger('submit');
                }, 300); // Wait a little bit before trigger add to cart handler
              }
            },
            error: function (response) {},
            complete: function () {
              form.removeClass('bh--checking');
            },
          });
        }
      });
    },
    loadMoreReviews: function () {
      var cpage = $('.next-reviews').data('cpage'),
        ajaxurl = $('.next-reviews').data('ajaxurl'),
        post_id = $('.next-reviews').data('post_id');
      $('.next-reviews a').click(function () {
        var button = $(this);
        cpage--;
        if (
          button.hasClass('bh-button-loading') ||
          button.hasClass('bh-button-disabled')
        ) {
          return false;
        }
        $.ajax({
          url: ajaxurl, // AJAX handler, declared before
          data: {
            action: 'reviews_loadmore', // wp_ajax_reviews_loadmore
            post_id: post_id, // the current post
            cpage: cpage, // current comment page
          },
          type: 'POST',
          beforeSend: function (xhr) {
            button.addClass('bh-button-loading'); // preloader here
          },
          success: function (data) {
            if (data) {
              $('ol.commentlist').append(data);
              button.removeClass('bh-button-loading');
              // if the last page, remove the button
              if (cpage == 1) {
                button.addClass('bh-button-disabled');
              }
            } else {
              button.addClass('bh-button-disabled');
            }
          },
        });
        return false;
      });
    },
  };
})(jQuery);
