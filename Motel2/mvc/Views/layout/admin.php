<?php require_once('./mvc/views/layout/headerAdmin.php'); ?>
<main>
  <?php require_once('./mvc/views/layout/alert.php'); ?>
  <?php echo $this->content ?>
</main>
<?php require_once('./mvc/views/layout/modal.php'); ?>
<?php require_once('./mvc/views/layout/footerAdmin.php'); ?>