<?php
class SuperadminController extends UserController
{
  public function canCreateUser()
  {
    return true;
  }

  public function canEditUser()
  {
    return true;
  }

  public function canDeleteUser()
  {
    return true;
  }
}
