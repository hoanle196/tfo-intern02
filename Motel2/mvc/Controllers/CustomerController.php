<?php
class CustomerController extends UserController
{
  public function canViewProfile()
  {
    return true;
  }

  public function canUpdateProfile()
  {
    return true;
  }

  public function canViewOrders()
  {
    return true;
  }

  public function canCreateOrder()
  {
    return true;
  }

  public function canUpdateOrder()
  {
    return true;
  }

  public function canCancelOrder()
  {
    return true;
  }
}
