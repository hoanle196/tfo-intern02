<?php
class Alert
{
  public static function notification($args = [])
  {
    $_SESSION[$args['status']] = $args['message'];
    $_SESSION['login'] = 'true';
    header("location:?c={$args['location']}");
    exit;
  }
}
