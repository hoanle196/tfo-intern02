<?php
require_once("./config.php");
require_once("./mvc/Core/DB.php");
require_once("./mvc/Core/Hooks.php");
require_once("./mvc/Helper/Helper.php");
require_once("./mvc/Helper/Alert.php");
require_once("./mvc/Core/App.php");
class MiddleWare extends Hooks
{
  public function __construct()
  {
    if (!isset($_SESSION['login'])) return new App('authentication');
    return new App();
  }
  public function checkRoles()
  {
    $conn = $this->connectDB();
    $sql = "SELECT * FROM users WHERE user_email = ? ";
    $conn->prepare($sql);
  }
}
