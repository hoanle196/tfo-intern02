<?php
class Hooks
{
    public $title = 'Student';
    public $content;
    public function connectDB()
    {
        $obj = new DB;
        return $obj->connect();
    }
    public function model($model)
    {
        require_once("./mvc/models/$model.php");
        return new $model;
    }
    public function view($view, $args = [])
    {
        ob_start();
        extract($args);
        $path = str_replace('.', '/', $view);
        require_once("./mvc/views/$path.php");
        $content = ob_get_clean();
        return $content;
    }
    public function layout($layout)
    {
        require_once("./mvc/views/layout/$layout.php");
    }
}
