<?php
class App
{
    public function __construct($c = null)
    {
        $classctl = $_GET['c'] ?? 'user';
        $function = $_GET['a'] ?? 'index';
        if ($c) {
            $classctl = $c;
        }
        $controller = ucfirst($classctl) . 'Controller'; //
        require_once("./mvc/Controllers/$controller.php");
        $objcontroller = new $controller();
        $objcontroller->$function();
    }
}
