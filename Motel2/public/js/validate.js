jQuery(document).ready(function($) {
    $('form').validate({
        rules: {
            name: {
                required: true,
                minlength: 5
            },
            date: 'required',
            address: 'required',
            email: {
                required: true,
                email: true
            },
            password: {
                minlength: 5,
                required: true
            },
            phone: {
                minlength: 10,
                required: true,
                digits: true,
                number: true
            },
            passwordConfirm: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            terms: {
                required: true
            },
            avatar: {
                required: true,
                extension: "jpg|png|jpeg"
            },
        }
    });
    $(document).on('blur', "#email", () => {
        const a = $('#email').val();
        fetch(`http://localhost/backend/QLSV-OOP/index.php?c=login&a=APICheck&email=${a}`)
            .then(d => d.json())
            .then(data => {
                if (data.count > 0) {
                    $('#email').after(`<label id="email-error" class="error" for="email">Email already exists</label>`);
                } else {
                    console.log('chua ton tai');
                }
            });
    });
})