<div class="container">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th class="col-md-2">Tên phòng</th>
        <th class="col-md-2">Giá phòng</th>
        <th class="col-md-3">Ngày bắt đầu</th>
        <th class="col-md-3">Ngày kết thúc</th>
        <th class="col-md-2">Trạng thái</th>
        <th class="col-md-2">action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Phòng A</td>
        <td>$100</td>
        <td>01/01/2022</td>
        <td>01/02/2022</td>
        <td><span class="badge badge-success">Đã đặt</span></td>
        <td><button class="btn btn-danger btn-sm" onclick="deleteRow(this)">Xóa</button></td>
      </tr>
      <tr>
        <td>Phòng B</td>
        <td>$150</td>
        <td>02/01/2022</td>
        <td>02/02/2022</td>
        <td><span class="badge badge-danger">Hết phòng</span></td>
        <td><button class="btn btn-danger btn-sm" onclick="deleteRow(this)">Xóa</button></td>
      </tr>
      <tr>
        <td>Phòng C</td>
        <td>$200</td>
        <td>03/01/2022</td>
        <td>03/02/2022</td>
        <td><span class="badge badge-warning">Chờ xác nhận</span></td>
        <td><button class="btn btn-danger btn-sm" onclick="deleteRow(this)">Xóa</button></td>
      </tr>
    </tbody>
  </table>
</div>