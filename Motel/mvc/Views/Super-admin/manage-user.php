<table class="table">
  <thead>
    <tr>
      <th>ID</th>
      <th>User name</th>
      <th>Email</th>
      <th>Address</th>
      <th>Phone</th>
      <th>Role</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $key => $value) {
    ?>
      <tr id="user-<?php echo $value['id']; ?>">
        <td><?php echo $value['id']; ?></td>
        <td><?php echo $value['name']; ?></td>
        <td><?php echo $value['email']; ?></td>
        <td><?php echo $value['address']; ?></td>
        <td><?php echo $value['phone']; ?></td>
        <td><?php echo $value['role']; ?></td>
        <td><span class="badge <?php echo $value['status'] == "1" ? 'badge-success' : 'badge badge-danger'; ?>"><?php echo $value['status'] == "1" ? "Hoạt động" : "Vô hiệu hóa"; ?></span></td>
        <td>
          <form action="?a=ApiChangeStatusUser" method="POST">
            <?php if ($value['status'] == "1") { ?>
              <input type="hidden" name="status" value="0">
              <input type="hidden" name="userId" value="<?php echo $value['id'] ?>">
              <button type="submit" class="btn btn-warning disable-btn" data-userid="<?php echo $value['id'] ?>">Vô hiệu hóa</button>
            <?php } else { ?>
              <input type="hidden" name="status" value="1">
              <input type="hidden" name="userId" value="<?php echo $value['id'] ?>">
              <button type="submit" class="btn btn-success enable-btn" data-userid="<?php echo $value['id'] ?>">Mở lại</button>
            <?php } ?>
          </form>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<? ?>