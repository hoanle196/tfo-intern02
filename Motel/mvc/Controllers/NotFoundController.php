<?php
class NotFoundController extends Hooks
{
  public function __construct()
  {
    $this->index();
  }

  public function index()
  {
    unset($_SESSION['login']);
    $this->title = '404';
    $this->layout('404page');
    // Alert::notification([
    //   "status" => "error",
    //   "message" => "Account has been locked",
    //   "location" => "",
    // ]);
  }
}
