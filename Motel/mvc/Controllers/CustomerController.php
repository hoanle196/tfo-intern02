<?php
class CustomerController extends Hooks
{
  public function index()
  {
    if (isset($_GET['searchSubmit'])) {
      $data = $this->getBySearch($_GET);
    } else {
      $data = $this->getListRoomModel();
    }
    $this->title = 'Motel Home';
    $this->content = $this->view('Client.index', compact('data'));
    $this->layout('client');
  }

  public function detailRoom()
  {
    $bookings = $this->fetchAllInTable('bookings');
    $this->title = 'details';
    $data = $this->getListRoomModel($_GET['id']);
    $infoUserPost = $this->fetchOneInTable('users', $data[0]['user_id']);
    $attr = $this->fetchInhWithParam('attributes', unserialize($data[0]['attribute']));
    $this->content = $this->view('Client.details', compact('data', 'attr', 'infoUserPost', 'bookings'));
    $this->layout('client');
  }

  public function booking()
  {
    if ($this->insertBooking($_POST)) {
      $bookingId = $this->conn->lastInsertId();
      $checkInCheckOut = $this->fetchOneInTable('bookings', $bookingId);
      $data = $this->getListRoomModel($_GET['id']);
      $attr = $this->fetchInhWithParam('attributes', unserialize($data[0]['attribute']));
      $infoUserPost = $this->fetchOneInTable('users', $data[0]['user_id']);
    }
    $this->title = 'order Motel';
    $this->content = $this->view('Client.order', compact('data', 'attr', 'infoUserPost', 'bookingId', 'checkInCheckOut'));
    $this->layout('client');
  }

  public function order()
  {
    if ($this->insertOrder($_POST)) {
      $data = $this->getListRoomModel($_GET['id']);
      $infoUserPost = $this->fetchOneInTable('users', $data[0]['user_id']);
      $bookingId = $_POST['bookingId'];
      $userOrder = $_SESSION['login'];
      $content = $this->view('Admin-motel.confirm-order', compact('data', 'infoUserPost', 'userOrder', 'bookingId'));
      $args = [
        'email' => $infoUserPost[0]['email'],
        'username' => $infoUserPost[0]['name'],
        'content' => $content,
        'subject' => 'Thư xác nhận đặt phòng'
      ];
      if (Helper::sendEmail($args)) {

        Alert::notification([
          "status" => "success",
          "message" => "Order and Send email success fully",
          "location" => "&a=myOrder",
        ]);
      } else {
        Alert::notification([
          "status" => "error",
          "message" => "send email fail",
          "location" => "&a=index",
        ]);
      }
    }
  }

  public function myOrder()
  {
    $this->title = 'my order';
    // $data = $this->getListRoomModel($_GET['id']);
    // $infoUserPostPost = $this->fetchOneInTable('users', $data[0]['user_id']);
    // $attr = $this->fetchInhWithParam('attributes', unserialize($data[0]['attribute']));
    $this->content = $this->view('Client.list-order');
    $this->layout('client');
  }

  public function ApiGetDataDistrict()
  {
    $this->getDataDistrict($_GET);
  }

  public function ApiGetDataWard()
  {
    $this->getDataWard($_GET);
  }

  public function logout()
  {
    unset($_SESSION['login']);
    Alert::notification([
      'status' => 'success',
      'message' => 'logout successfully',
      'location' => 'authentication&a=index',
    ]);
  }
}
