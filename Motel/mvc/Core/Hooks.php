<?php
class Hooks
{
    public $title = 'Student';
    public $content;
    public $conn;
    public function connectDB()
    {
        $obj = new DB;
        $this->conn = $obj->connect();
        return $this->conn;
    }
    public function model($model)
    {
        require_once("./mvc/models/$model.php");
        return new $model;
    }
    public function view($view, $args = [])
    {
        ob_start();
        extract($args);
        $path = str_replace('.', '/', $view);
        require_once("./mvc/views/$path.php");
        $content = ob_get_clean();
        return $content;
    }
    public function layout($layout)
    {
        require_once("./mvc/views/layout/$layout.php");
    }
    public function getDataDistrict($data)
    {
        $this->connectDB();
        extract($data);
        $sql = "SELECT * FROM districts WHERE districts.province_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$province]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function updateStatusUser($data)
    {
        $this->connectDB();
        extract($data);
        $sql = "UPDATE users SET status= ? WHERE id= ? ";
        $stmt = $this->conn->prepare($sql);
        return $stmt->execute([$status, $userId]);
    }

    public function getDataWard($data)
    {
        $this->connectDB();
        extract($data);
        $sql = "SELECT * FROM wards WHERE wards.district_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$ward]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        header('Content-Type: application/json');
        echo json_encode($result);
    }
    public function getDataImage($id)
    {
        $this->connectDB();
        $sql = "SELECT * FROM images WHERE room_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchAllInTable($table)
    {
        $this->connectDB();
        $sql = "SELECT * FROM $table";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchOneInTable($table, $id)
    {
        $this->connectDB();
        $sql = "SELECT * FROM $table WHERE id = ? ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchInhWithParam($table, $params)
    {
        $this->connectDB();
        $sql = "SELECT * FROM $table WHERE id IN (" . implode(',', $params) . ")";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function destroyWithId($table, $id)
    {
        $this->connectDB();
        Helper::deleteFileUpload($this->conn, $id);
        $sql = "DELETE FROM $table WHERE id = ? ;";
        $stmt = $this->conn->prepare($sql);
        return $stmt->execute([$id]);
    }

    public function getListRoomModel($condition = null)
    {
        $this->connectDB();
        if ($condition) {
            $sql = Helper::createQueryGet($condition);
        } else {
            $sql = Helper::createQueryGet();
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getBySearch($data)
    {
        $this->connectDB();
        $sql = Helper::createQuerySearch($data);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insertBooking($data)
    {
        $this->connectDB();
        extract($data);
        $sql = "INSERT INTO bookings ( bookings.start_date, bookings.end_date, bookings.room_id, bookings.user_id )
        VALUES
            ( ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($sql);
        return $stmt->execute([$start, $end, $_GET['id'], $_SESSION['login']['id']]);
    }

    public function insertOrder($data)
    {
        $this->connectDB();
        extract($data);
        $sql = "INSERT INTO orders ( orders.booking_id, orders.price )
        VALUES
            ( ?, ?)";
        $stmt = $this->conn->prepare($sql);
        return $stmt->execute([$bookingId, $price]);
    }

    public function reportOrder($data)
    {
        extract($data);
        $this->connectDB();
        if ($status === 'resolve') {
            $sql = "UPDATE orders SET orders.`status` = 1 WHERE orders.id = ?";
            $stmt = $this->conn->prepare($sql);
            if ($stmt->execute([$idRoomOrder])) {
                $args = [
                    'email' => $email,
                    'username' => $name,
                    'content' => "<h1>Chào bạn {$name} Bạn đã đặt phòng thành công, vui lòng tới đúng ngày để nhận phòng nhé! cám ơn bạn đã sử dụng dịch vụ của chúng tôi </h1>",
                    'subject' => 'Thông báo kết quả đặt phòng'
                ];
                if (Helper::sendEmail($args)) {
                    return $this->updateStatusRoom($idRoomOrder);
                }
                return false;
            }
        } else {
            $sql = " DELETE from bookings WHERE id = ?";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute([$bookingId]);
            $args = [
                'email' => $email,
                'username' => $name,
                'content' => "<h1>Chào bạn {$name} Phòng của bạn đã được người khác đặt, vui lòng chọn phòng khác nhé! cám ơn bạn đã sử dụng dịch vụ của chúng tôi </h1>",
                'subject' => 'Thông báo kết quả đặt phòng'
            ];
            if (Helper::sendEmail($args)) {
                return true;
            }
            return false;
        }
    }
    public function updateStatusRoom($idRoom)
    {
        $sql = "UPDATE rooms SET rooms.`status` = 0 WHERE rooms.id = ?";
        $stmt = $this->conn->prepare($sql);
        return $stmt->execute([$idRoom]);
    }
}
