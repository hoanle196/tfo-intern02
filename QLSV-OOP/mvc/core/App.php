<?php
class App
{
    public function __construct()
    {
        $classctl = $_GET['c'] ?? 'student';
        $function = $_GET['a'] ?? 'index';
        $controller = ucfirst($classctl) . 'Controller'; // StudentController
        require_once("./mvc/controllers/$controller.php");
        $objcontroller = new $controller();
        $objcontroller->$function();
    }
}
