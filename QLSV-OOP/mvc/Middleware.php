<?php
require_once("./config.php");
require_once("./mvc/core/DB.php");
require_once("./mvc/core/Hooks.php");
require_once("./mvc/core/App.php");
class MiddleWare
{
  public function __construct()
  {
    new App();
    if (isset($_SESSION['token'])) {
      $login = new Login;
      $login->index();
    }
  }
}
