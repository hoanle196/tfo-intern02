$('button.delete').click(function(e) {
    var dataHref = $(this).attr('data-href');
    $('#exampleModal a').attr('href', dataHref);
});
const filesUpload = document.querySelector(".uploadfile");
const input = document.querySelector("#uploadfile")
filesUpload.addEventListener("click", function() {
    input.click();
});
$(".uploadfile").change(function(event) {
    $('.image').remove();
    const {
        files
    } = event.target;
    // debugger;
   for( arr of files) {
    const url = window.URL.createObjectURL(arr);
        $('.uploadfile').after(`
        <img src="${url}" class = "image" alt="">`);
   }
});
$('form').validate({
    rules: {
      name: 'required',
      date: 'required',
      address: 'required',
    //   password: {
    //     minlength: 5,
    //     required: true
    //   },
      avatar: {
        required: true,
        extension: "jpg|png|jpge"
      },
    }
  });